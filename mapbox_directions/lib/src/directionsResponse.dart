// To parse this JSON data, do
//
//     final directionsResponse = directionsResponseFromJson(jsonString);

import 'dart:convert';
import 'coordinate.dart';
DirectionsResponse directionsResponseFromJson(String str) {
  final jsonData = json.decode(str);
  return DirectionsResponse.fromJson(jsonData);
}

String directionsResponseToJson(DirectionsResponse data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class DirectionsResponse {
  List<Route> routes;
  List<Waypoint> waypoints;
  String code;
  String uuid;

  DirectionsResponse({
    this.routes,
    this.waypoints,
    this.code,
    this.uuid,
  });

  factory DirectionsResponse.fromJson(Map<String, dynamic> json) => new DirectionsResponse(
    routes: json["routes"] == null ? null : new List<Route>.from(json["routes"].map((x) => Route.fromJson(x))),
    waypoints: json["waypoints"] == null ? null : new List<Waypoint>.from(json["waypoints"].map((x) => Waypoint.fromJson(x))),
    code: json["code"] == null ? null : json["code"],
    uuid: json["uuid"] == null ? null : json["uuid"],
  );

  Map<String, dynamic> toJson() => {
    "routes": routes == null ? null : new List<dynamic>.from(routes.map((x) => x.toJson())),
    "waypoints": waypoints == null ? null : new List<dynamic>.from(waypoints.map((x) => x.toJson())),
    "code": code == null ? null : code,
    "uuid": uuid == null ? null : uuid,
  };
}
class Geometry {
  List coordinates;
  String type;
  Map linestring;
  String polyline;


  Geometry({
    this.coordinates,
    this.type,
    this.linestring,
    this.polyline,
  });

  Geometry.fromJson(Map<String, dynamic> json) {
    if(json['geometry'] is String){
      type ='polyline';
      polyline = json['geometry'];
    } else {
      type = 'linestring';
      coordinates = json['geometry']['coordinates'].map((point) => Coordinate(point[1],point[0])).toList();
    }
  }
  Map<String, dynamic> toJson() => {
    "type":type,
    "polyline": polyline,
    "coordinates": coordinates,
    'linestring':linestring,

  };

}
class Route {
  Geometry geometry;
  List<Leg> legs;
  String weightName;
  double weight;
  double duration;
  double distance;

  Route({
    this.geometry,
    this.legs,
    this.weightName,
    this.weight,
    this.duration,
    this.distance,
  });

  factory Route.fromJson(Map<String, dynamic> json) => new Route(
    geometry: json["geometry"] == null ? null : Geometry.fromJson(json),
    legs: json["legs"] == null ? null : new List<Leg>.from(json["legs"].map((x) => Leg.fromJson(x))),
    weightName: json["weight_name"] == null ? null : json["weight_name"],
    weight: json["weight"] == null ? null : json["weight"].toDouble(),
    duration: json["duration"] == null ? null : json["duration"].toDouble(),
    distance: json["distance"] == null ? null : json["distance"].toDouble(),
  );

  Map<String, dynamic> toJson() => {
    "geometry": geometry == null ? null : geometry.toJson(),
    "legs": legs == null ? null : new List<dynamic>.from(legs.map((x) => x.toJson())),
    "weight_name": weightName == null ? null : weightName,
    "weight": weight == null ? null : weight,
    "duration": duration == null ? null : duration,
    "distance": distance == null ? null : distance,
  };
}

class Leg {
  String summary;
  double weight;
  double duration;
  List<Step> steps;
  double distance;

  Leg({
    this.summary,
    this.weight,
    this.duration,
    this.steps,
    this.distance,
  });

  factory Leg.fromJson(Map<String, dynamic> json) => new Leg(
    summary: json["summary"] == null ? null : json["summary"],
    weight: json["weight"] == null ? null : json["weight"].toDouble(),
    duration: json["duration"] == null ? null : json["duration"].toDouble(),
    steps: json["steps"] == null ? null : new List<Step>.from(json["steps"].map((x) => Step.fromJson(x))),
    distance: json["distance"] == null ? null : json["distance"].toDouble(),
  );

  Map<String, dynamic> toJson() => {
    "summary": summary == null ? null : summary,
    "weight": weight == null ? null : weight,
    "duration": duration == null ? null : duration,
    "steps": steps == null ? null : new List<dynamic>.from(steps.map((x) => x.toJson())),
    "distance": distance == null ? null : distance,
  };
}

class Step {
  List<Intersection> intersections;
  String drivingSide;
  Geometry geometry;
  String mode;
  Maneuver maneuver;
  String ref;
  double weight;
  double duration;
  String name;
  double distance;
  List<VoiceInstruction> voiceInstructions;
  List<BannerInstruction> bannerInstructions;

  Step({
    this.intersections,
    this.drivingSide,
    this.geometry,
    this.mode,
    this.maneuver,
    this.ref,
    this.weight,
    this.duration,
    this.name,
    this.distance,
    this.voiceInstructions,
    this.bannerInstructions,
  });

  factory Step.fromJson(Map<String, dynamic> json) => new Step(
    intersections: json["intersections"] == null ? null : new List<Intersection>.from(json["intersections"].map((x) => Intersection.fromJson(x))),
    drivingSide: json["driving_side"] == null ? null : json["driving_side"],
    geometry: json["geometry"] == null ? null : Geometry.fromJson(json),
    mode: json["mode"] == null ? null : json["mode"],
    maneuver: json["maneuver"] == null ? null : Maneuver.fromJson(json["maneuver"]),
    ref: json["ref"] == null ? null : json["ref"],
    weight: json["weight"] == null ? null : json["weight"].toDouble(),
    duration: json["duration"] == null ? null : json["duration"].toDouble(),
    name: json["name"] == null ? null : json["name"],
    distance: json["distance"] == null ? null : json["distance"].toDouble(),
    voiceInstructions: json["voiceInstructions"] == null ? null : new List<VoiceInstruction>.from(json["voiceInstructions"].map((x) => VoiceInstruction.fromJson(x))),
    bannerInstructions: json["bannerInstructions"] == null ? null : new List<BannerInstruction>.from(json["bannerInstructions"].map((x) => BannerInstruction.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "intersections": intersections == null ? null : new List<dynamic>.from(intersections.map((x) => x.toJson())),
    "driving_side": drivingSide == null ? null : drivingSide,
    "geometry": geometry == null ? null : geometry.toJson(),
    "mode": mode == null ? null : mode,
    "maneuver": maneuver == null ? null : maneuver.toJson(),
    "ref": ref == null ? null : ref,
    "weight": weight == null ? null : weight,
    "duration": duration == null ? null : duration,
    "name": name == null ? null : name,
    "distance": distance == null ? null : distance,
    "voiceInstructions": voiceInstructions == null ? null : new List<dynamic>.from(voiceInstructions.map((x) => x.toJson())),
    "bannerInstructions": bannerInstructions == null ? null : new List<dynamic>.from(bannerInstructions.map((x) => x.toJson())),
  };
}

class BannerInstruction {
  double distanceAlongGeometry;
  Ary primary;
  Ary secondary;
  dynamic then;

  BannerInstruction({
    this.distanceAlongGeometry,
    this.primary,
    this.secondary,
    this.then,
  });

  factory BannerInstruction.fromJson(Map<String, dynamic> json) => new BannerInstruction(
    distanceAlongGeometry: json["distanceAlongGeometry"] == null ? null : json["distanceAlongGeometry"].toDouble(),
    primary: json["primary"] == null ? null : Ary.fromJson(json["primary"]),
    secondary: json["secondary"] == null ? null : Ary.fromJson(json["secondary"]),
    then: json["then"],
  );

  Map<String, dynamic> toJson() => {
    "distanceAlongGeometry": distanceAlongGeometry == null ? null : distanceAlongGeometry,
    "primary": primary == null ? null : primary.toJson(),
    "secondary": secondary == null ? null : secondary.toJson(),
    "then": then,
  };
}

class Ary {
  String text;
  List<Component> components;
  String type;
  String modifier;

  Ary({
    this.text,
    this.components,
    this.type,
    this.modifier,
  });

  factory Ary.fromJson(Map<String, dynamic> json) => new Ary(
    text: json["text"] == null ? null : json["text"],
    components: json["components"] == null ? null : new List<Component>.from(json["components"].map((x) => Component.fromJson(x))),
    type: json["type"] == null ? null : json["type"],
    modifier: json["modifier"] == null ? null : json["modifier"],
  );

  Map<String, dynamic> toJson() => {
    "text": text == null ? null : text,
    "components": components == null ? null : new List<dynamic>.from(components.map((x) => x.toJson())),
    "type": type == null ? null : type,
    "modifier": modifier == null ? null : modifier,
  };
}

class Component {
  String text;

  Component({
    this.text,
  });

  factory Component.fromJson(Map<String, dynamic> json) => new Component(
    text: json["text"] == null ? null : json["text"],
  );

  Map<String, dynamic> toJson() => {
    "text": text == null ? null : text,
  };
}

class Intersection {
  int out;
  List<bool> entry;
  List<int> bearings;
  List<double> location;
  int intersectionIn;

  Intersection({
    this.out,
    this.entry,
    this.bearings,
    this.location,
    this.intersectionIn,
  });

  factory Intersection.fromJson(Map<String, dynamic> json) => new Intersection(
    out: json["out"] == null ? null : json["out"],
    entry: json["entry"] == null ? null : new List<bool>.from(json["entry"].map((x) => x)),
    bearings: json["bearings"] == null ? null : new List<int>.from(json["bearings"].map((x) => x)),
    location: json["location"] == null ? null : new List<double>.from(json["location"].map((x) => x.toDouble())),
    intersectionIn: json["in"] == null ? null : json["in"],
  );

  Map<String, dynamic> toJson() => {
    "out": out == null ? null : out,
    "entry": entry == null ? null : new List<dynamic>.from(entry.map((x) => x)),
    "bearings": bearings == null ? null : new List<dynamic>.from(bearings.map((x) => x)),
    "location": location == null ? null : new List<dynamic>.from(location.map((x) => x)),
    "in": intersectionIn == null ? null : intersectionIn,
  };
}

class Maneuver {
  int bearingAfter;
  int bearingBefore;
  List<double> location;
  String modifier;
  String type;
  String instruction;

  Maneuver({
    this.bearingAfter,
    this.bearingBefore,
    this.location,
    this.modifier,
    this.type,
    this.instruction,
  });

  factory Maneuver.fromJson(Map<String, dynamic> json) => new Maneuver(
    bearingAfter: json["bearing_after"] == null ? null : json["bearing_after"],
    bearingBefore: json["bearing_before"] == null ? null : json["bearing_before"],
    location: json["location"] == null ? null : new List<double>.from(json["location"].map((x) => x.toDouble())),
    modifier: json["modifier"] == null ? null : json["modifier"],
    type: json["type"] == null ? null : json["type"],
    instruction: json["instruction"] == null ? null : json["instruction"],
  );

  Map<String, dynamic> toJson() => {
    "bearing_after": bearingAfter == null ? null : bearingAfter,
    "bearing_before": bearingBefore == null ? null : bearingBefore,
    "location": location == null ? null : new List<dynamic>.from(location.map((x) => x)),
    "modifier": modifier == null ? null : modifier,
    "type": type == null ? null : type,
    "instruction": instruction == null ? null : instruction,
  };
}

class VoiceInstruction {
  double distanceAlongGeometry;
  String announcement;
  String ssmlAnnouncement;

  VoiceInstruction({
    this.distanceAlongGeometry,
    this.announcement,
    this.ssmlAnnouncement,
  });

  factory VoiceInstruction.fromJson(Map<String, dynamic> json) => new VoiceInstruction(
    distanceAlongGeometry: json["distanceAlongGeometry"] == null ? null : json["distanceAlongGeometry"].toDouble(),
    announcement: json["announcement"] == null ? null : json["announcement"],
    ssmlAnnouncement: json["ssmlAnnouncement"] == null ? null : json["ssmlAnnouncement"],
  );

  Map<String, dynamic> toJson() => {
    "distanceAlongGeometry": distanceAlongGeometry == null ? null : distanceAlongGeometry,
    "announcement": announcement == null ? null : announcement,
    "ssmlAnnouncement": ssmlAnnouncement == null ? null : ssmlAnnouncement,
  };
}

class Waypoint {
  String name;
  List<double> location;

  Waypoint({
    this.name,
    this.location,
  });

  factory Waypoint.fromJson(Map<String, dynamic> json) => new Waypoint(
    name: json["name"] == null ? null : json["name"],
    location: json["location"] == null ? null : new List<double>.from(json["location"].map((x) => x.toDouble())),
  );

  Map<String, dynamic> toJson() => {
    "name": name == null ? null : name,
    "location": location == null ? null : new List<dynamic>.from(location.map((x) => x)),
  };
}
