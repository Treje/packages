import 'package:http/http.dart' as http;
import './directionsResponse.dart';
import './coordinate.dart';
enum Profile {
  driving,
  walking,
  cycling,
  drivingTraffic
}
enum GeometryType {
  geojson,
  polyline,
  polyline6
}
class MapboxDirections {
    static String rateLimit = '300 requests per minute';
    static String maxDistance  = '10000Km';
    String token = '';
    String version = 'v5';
    ///accessToken - mapbox api token ||
    ///v - api version
    MapboxDirections({
      accessToken,///mapbox access token
      v = 'v5'}){
      if(accessToken != null){
        token = accessToken;
      }
      if(v != null){
        version = v;
      }
    }
    void setToken(t){
        token = t;
    }
    String setProfile(Profile profile){
      switch(profile){
        case Profile.drivingTraffic:
          return 'mapbox/driving-trafffic';
        case Profile.driving:
          return 'mapbox/driving';
        case Profile.cycling:
          return 'mapbox/cycling';
        case Profile.walking:
          return 'mapbox/walking';
        default:
           return 'mapbox/driving';
      }
    }

    setGeometry(GeometryType geometry){
      switch(geometry){
        case GeometryType.geojson:
          return 'geojson';
        case GeometryType.polyline:
          return 'polyline';
        case GeometryType.polyline6:
          return 'polyline6';
        default:
          return 'polyline';
      }
    }

    buildCoordinates(List<Coordinate> waypoints){
      String coordinates = '';
      for(int i =0; i< waypoints.length; i++){
        var point = waypoints[i];
        coordinates.trim().isNotEmpty?
            coordinates = '$coordinates;${point.latitude},${point.longitude}'
            :
        coordinates = '${point.latitude},${point.longitude}';
      }
      return coordinates;
    }

   Future<DirectionsResponse> get(List<Coordinate> waypoints,{
      Profile profile = Profile.driving,
      alternatives = false,
      annotations,
      approaches,
      bannerInstructions,
      bearing,
      continueStraight,
      exclude,
      GeometryType geometry = GeometryType.geojson,
      language,
      overview,
      radiuses,
      roundaboutExits,
      steps= true,
      voiceInstructions = true,
      voiceUnits,
      waypointNames,
      accessToken = ''
    }) async {

      switch(profile){
        case Profile.drivingTraffic:
          if(waypoints.length > 25){
            print('Error, waypoints can be max 3');
            return null;
          }
          break;
        default:
          if(waypoints.length > 25){
            print('Error, waypoints can be max 25');
            return null;
          }

      }
      if(token == null &&  accessToken == null){
        print('Token is not set');
            return null;
      }
      print('$token $accessToken');
      Map<String, String> queryParams = {
        'access_token': token ?? accessToken
      };


      print('https://api.mapbox.com/directions/$version/${setProfile(profile)}/${buildCoordinates(waypoints)}?access_token=$token');
      http.Response response = await http.get('https://api.mapbox.com/directions/$version/${setProfile(profile)}/${buildCoordinates(waypoints)}?access_token=$token&geometries=${setGeometry(geometry)}&steps=$steps&voice_instructions=$voiceInstructions');
      print(response.statusCode);
      print(response.reasonPhrase);
      print(response.body);

      try {
      DirectionsResponse directionsResponse =  directionsResponseFromJson(response.body);
      return directionsResponse;

      }catch(e){
        print(e);
      }
      return null;
    }
}
